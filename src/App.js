import React, {useState, useEffect} from 'react';
import Card from "./components/Card";

const App = () => {
    const [contacts, setContacts] = useState([]);
    const [sortedList, setSortedList] = useState([]);
    const [toggle, setToggle] = useState(false);
    const [search, setSearch] = useState('');


    useEffect(() => {
        fetch('https://cors-anywhere.herokuapp.com/https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(resp => resp.json())
            .then(data => {
                setContacts(data);
                setSortedList(data);
            })
            .catch(error => console.log(error));
    }, []);

    useEffect(() => {
        if (search.length === 0) {
            setSortedList(prev => {
                const array = contacts.sort((a, b) => {
                    const lastA = a.last_name.toUpperCase();
                    const lastB = b.last_name.toUpperCase();
                    if (lastA < lastB) {
                        return -1;
                    }
                    if (lastA > lastB) {
                        return 1;
                    }
                    return 0;
                });
                return array;
            });
        } else {
            setSortedList(prev => {
                const array = contacts.sort((a, b) => {
                    const lastA = a.last_name.toUpperCase();
                    const lastB = b.last_name.toUpperCase();
                    if (lastA < lastB) {
                        return -1;
                    }
                    if (lastA > lastB) {
                        return 1;
                    }
                    return 0;
                });
                const filterArray = array.filter(word => {
                    const firstName = word.first_name.toUpperCase();
                    const lastName = word.last_name.toUpperCase();
                    return firstName == search || lastName == search
                })
                return filterArray;
            });
        }
    }, [contacts, search]);

    const handleClick = (event) => {
        toggle === false ? setToggle(true) : setToggle(false);
        console.log(event.target.getAttribute('data-key'));
    };


    return (
        <div className="app container">
            <header className="app-header">
                <h1 className='header-title'>
                    Contacts
                </h1>
            </header>
            <section className='main'>
                <div className='main-search'>
                    <input type="text" name='search' placeholder='Search'
                           className='search-input'
                           value={search} onChange={e => setSearch(e.target.value.toUpperCase())}/>
                </div>
                {contacts.length === 0
                    ?
                    <div className='main-list'>Loading...</div>
                    :
                    <div className='main-list'>
                        {sortedList.map(contact => {
                            return (
                                <Card
                                    key={contact.id}
                                    avatar={contact.avatar}
                                    firstName={contact.first_name}
                                    lastName={contact.last_name}
                                    email={contact.email}
                                    check={<input
                                        data-key={contact.id}
                                        name="isGoing"
                                        type="checkbox"
                                        onChange={handleClick}
                                    />}
                                />
                            )
                        })}
                    </div>
                }
            </section>
        </div>
    );
};

export default App;
