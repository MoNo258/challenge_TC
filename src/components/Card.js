import React from 'react';

const Card = ({avatar, firstName, lastName, email, check}) => {


    return (
        <div className='card border-dark mb-3' >
            <div className=' card-header'>
                <img className='card-img-top' src={avatar} alt="contact-avatar"/>
            </div>
            <div className='card-body text-dark'>
                <h5 className='card-title'>
                    {`${firstName} ${lastName}`}
                </h5>
                <p className='card-text'>
                    {email}
                </p>
                <div className='card-checkbox'>
                    {check}
                </div>
            </div>
        </div>
    );
};

export default Card;
